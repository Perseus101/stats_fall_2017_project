import tensorflow as tf
import numpy as np

from network import Network
from data import read_csv, parse_split_and_normalize

import random

def main():
    DATA_FILE = "wdbc.data"
    INPUT_FEATURES = 30
    OUTPUT_CLASSES = 2
    EPOCHS = 500

    str_array = read_csv(DATA_FILE)

    net_types = {
        "triangular": [
            ('fc1',128, tf.nn.tanh),
            ('fc2', 64, tf.nn.tanh),
            ('fc3', 32, tf.nn.tanh),
            ('fc4', 16, tf.nn.tanh),
            ('fc5', 8, tf.nn.tanh),
            ('fc6', 4, tf.nn.tanh),
            ('out', 2, tf.nn.tanh)
        ],
        "deep_and_wide": [
            ('fc1', 42, tf.nn.tanh),
            ('fc2', 42, tf.nn.tanh),
            ('fc3', 42, tf.nn.tanh),
            ('fc4', 42, tf.nn.tanh),
            ('fc5', 42, tf.nn.tanh),
            ('fc6', 42, tf.nn.tanh),
            ('out', 2, tf.nn.tanh)
        ],
        "shallow": [
            ('fc1', 128, tf.nn.tanh),
            ('fc2', 128, tf.nn.tanh),
            ('out', 2, tf.nn.tanh)
        ],
        "deep_and_narrow": [ # AKA square
            ('fc1', 16, tf.nn.tanh),
            ('fc2', 16, tf.nn.tanh),
            ('fc3', 16, tf.nn.tanh),
            ('fc4', 16, tf.nn.tanh),
            ('fc5', 16, tf.nn.tanh),
            ('fc6', 16, tf.nn.tanh),
            ('fc7', 16, tf.nn.tanh),
            ('fc8', 16, tf.nn.tanh),
            ('fc9', 16, tf.nn.tanh),
            ('fc10', 16, tf.nn.tanh),
            ('fc11', 16, tf.nn.tanh),
            ('fc12', 16, tf.nn.tanh),
            ('fc13', 16, tf.nn.tanh),
            ('fc14', 16, tf.nn.tanh),
            ('fc15', 16, tf.nn.tanh),
            ('fc16', 16, tf.nn.tanh),
            ('out', 2, tf.nn.tanh)
        ]
    }

    optimizers = {
        "SGD": tf.train.GradientDescentOptimizer(learning_rate=0.0001),
        "Adam": tf.train.AdamOptimizer(learning_rate=0.0001),
        "AdaGrad": tf.train.AdagradOptimizer(learning_rate=0.001),
        "Momentum": tf.train.MomentumOptimizer(learning_rate=0.001, momentum=0.001),
        "RMSProp": tf.train.RMSPropOptimizer(learning_rate=0.001)
    }
    pairs = []
    for net_name, params in net_types.items():
        for op_name, optimizer in optimizers.items():
            pairs.append((net_name + "_" + op_name + "_0", net_name, op_name))
            pairs.append((net_name + "_" + op_name + "_1", net_name, op_name))
            pairs.append((net_name + "_" + op_name + "_2", net_name, op_name))
    random.shuffle(pairs)

    results = []

    for i, (name, net_name, op_name) in enumerate(pairs):
        np.random.shuffle(str_array)
        train_labels, test_labels, train_data, test_data = parse_split_and_normalize(str_array)

        tf.reset_default_graph()
        with tf.Session() as sess:
            print("Creating {} network, optimizing with {}".format(net_name, op_name))
            network = Network(name, sess, INPUT_FEATURES, OUTPUT_CLASSES, params, optimizer, summary=True)
            sess.run(tf.global_variables_initializer())
            for epoch in range(EPOCHS):
                # Test accuracy
                accuracy = network.evaluate_accuracy(sess, test_data, test_labels)
                print("\tAccuracy at epoch {} is {}".format(epoch, accuracy))

                # Train the network
                network.train(sess, train_data, train_labels)
            accuracy = network.evaluate_accuracy(sess, test_data, test_labels)
            print("\tFinal accuracy is {}".format(accuracy))
            results.append((net_name, op_name, str(accuracy), str(i + 1)))

    with open("results.txt", "w") as f:
        for result in results:
            f.write(" ".join(result))
            f.write("\n")

if __name__ == "__main__":
    main()