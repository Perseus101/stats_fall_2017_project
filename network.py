import tensorflow as tf

from os.path import join

class Network(object):
    """
    An object containing a neural network.

    Params:
        network_name: A name for the model (used to specify the save location for the model summaries)
        session: An instance of tf.Session
        input_shape: The input shape for the network
        output_shape: The number of output classes
        network_format: A list containing tuples with information about each layer.
            The tuple items must be as follows:
                a string which specifies the name of the layer
                an integer specifying the size of the layer
                an activation function for the layer (i.e. tf.nn.relu, tf.nn.tanh, etc, or tf.identity for no activation)
        optimizer: The optimizer to use for the neural net.
            Must be a subclass of tf.Optimizer
    """
    def __init__(self, network_name, session, input_shape, output_shape, network_format, optimizer, summary=False):
        self.summaries = []
        layer = None
        input_layer = tf.placeholder(tf.float32, [None, input_shape])
        prev_shape = input_layer.shape[1].value
        for name, shape, activation in network_format:
            with tf.name_scope(name):
                weight = self.weight_variable([prev_shape, shape], summary)
                bias   = self.bias_variable([shape], summary)

                if layer == None:
                    tmp = activation(tf.add(tf.matmul(input_layer, weight), bias))
                else:
                    tmp = activation(tf.add(tf.matmul(layer, weight), bias))
                layer = tmp
                prev_shape = shape

        self.input = input_layer
        self.labels_in = tf.placeholder(tf.int64, [None])
        self.labels = tf.one_hot(self.labels_in, output_shape)
        self.output = tf.nn.softmax(layer)
        self.output_class = tf.argmax(self.output, axis = 1)

        with tf.name_scope('optimization'):
            self.loss = tf.reduce_sum(tf.square(self.output - self.labels))
            self.summaries.append(tf.summary.scalar('loss', self.loss))
            self.accuracy = tf.reduce_mean(tf.cast(tf.equal(self.output_class, self.labels_in), tf.float32))
            self.accuracy_summary = tf.summary.scalar('accuracy', self.accuracy)
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            self.optimizer = optimizer.minimize(self.loss, global_step=self.global_step)

        self.summary_writer = tf.summary.FileWriter(join("summary", network_name), session.graph)
        self.summary = tf.summary.merge(self.summaries)

    def evaluate(self, session, data):
        return session.run(self.output_class, feed_dict={self.input: data})

    def evaluate_accuracy(self, session, data, labels):
        accuracy, summary, step = session.run([self.accuracy, self.accuracy_summary, self.global_step], feed_dict={self.input: data, self.labels_in:labels})
        self.summary_writer.add_summary(summary, step)
        return accuracy

    def train(self, session, data, labels):
        _, summary, step = session.run([self.optimizer, self.summary, self.global_step],
                    feed_dict={self.input: data, self.labels_in: labels})
        self.summary_writer.add_summary(summary, step)

    def weight_variable(self, shape, summary=False):
        with tf.name_scope('weight'):
            initial = tf.truncated_normal(shape, stddev=0.1)
            var = tf.Variable(initial, name='weight')
            if summary:
                self.attach_summaries(var)
        return var

    def bias_variable(self, shape, summary=False):
        with tf.name_scope('bias'):
            initial = tf.constant(0.1, shape=shape)
            var = tf.Variable(initial, name='bias')
            if summary:
                self.attach_summaries(var)
        return var

    def attach_summaries(self, var):
        with tf.name_scope('summaries'):
            mean = tf.reduce_mean(var)
            self.summaries.append(tf.summary.scalar('mean', mean))
            with tf.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
            self.summaries.append(tf.summary.scalar('stddev', stddev))
            self.summaries.append(tf.summary.scalar('max', tf.reduce_max(var)))
            self.summaries.append(tf.summary.scalar('min', tf.reduce_min(var)))
            self.summaries.append(tf.summary.histogram('histogram', var))