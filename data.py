import pandas as pd
import numpy as np

def read_csv(path):
    return pd.read_csv(path, sep=',', header=None, dtype=str).values

def parse_str_array(str_array):
    labels = (str_array[:, 1] == 'M').astype(np.int32)
    unnormalized_data = str_array[:, 2:].astype(np.float)
    return (labels, unnormalized_data)

def get_means_and_stds(unnormalized_data):
    means = np.mean(unnormalized_data, axis=0)
    stds = np.std(unnormalized_data, axis=0)
    return (means, stds)

def normalize_data(unnormalized_data, means, stds):
    return (unnormalized_data - means) / stds

def read_parse_and_normalize(path):
    str_array = read_csv(path)
    labels, unnormalized_data = parse_str_array(str_array)
    means, stds = get_means_and_stds(unnormalized_data)
    data = normalize_data(unnormalized_data, means, stds)
    return (labels, data)

def parse_split_and_normalize(str_array, proportion_for_training=0.75):
    split_point = int(proportion_for_training*str_array.shape[0])
    train_labels, train_unnormalized_data = parse_str_array(str_array[:split_point, :])
    test_labels, test_unnormalized_data = parse_str_array(str_array[split_point:, :])

    means, stds = get_means_and_stds(train_unnormalized_data)

    train_data = normalize_data(train_unnormalized_data, means, stds)
    test_data = normalize_data(test_unnormalized_data, means, stds)
    return (train_labels, test_labels, train_data, test_data)